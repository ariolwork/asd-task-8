# Key Word in Context (KWIC). Abstract Data Types Method

class KWIC:
    def __init__(self, text):
        self.text = text
    
    def process(self):
        keywords = self.extract_keywords()
        context = self.generate_context(keywords)
        return context
    
    def extract_keywords(self):
        words = self.text.split()
        common_words = ["a", "an", "the", "in", "on", "of"]
        keywords = [word for word in words if word.lower() not in common_words]
        return keywords
    
    def generate_context(self, keywords):
        context = []
        for keyword in keywords:
            index = self.text.lower().index(keyword.lower())
            start_index = max(0, index - 10)
            end_index = min(len(self.text), index + len(keyword) + 10)
            keyword_context = self.text[start_index:end_index]
            context.append((keyword, keyword_context))
        return context

# Example usage
text = "This is a sample text. It contains keywords for testing."
kwic = KWIC(text)
result = kwic.process()
for keyword, context in result:
    print(f"Keyword: {keyword}")
    print(f"Context: {context}")
    print()