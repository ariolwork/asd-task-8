# Eight Queens (8Q). Observer/Listener Method

class Queen:
    def __init__(self, row, column):
        self.row = row
        self.column = column

    def is_attack(self, other_queen):
        if self.row == other_queen.row or self.column == other_queen.column:
            return True
        if abs(self.row - other_queen.row) == abs(self.column - other_queen.column):
            return True
        return False


class Board:
    def __init__(self, size):
        self.size = size
        self.queens = []
        self.listeners = []

    def add_listener(self, listener):
        self.listeners.append(listener)

    def remove_listener(self, listener):
        self.listeners.remove(listener)

    def notify_listeners(self, solutions):
        for listener in self.listeners:
            listener(solutions)

    def solve(self):
        self.place_queen(0)

    def place_queen(self, row):
        if row == self.size:
            self.notify_listeners([[queen.column for queen in self.queens]])
            return

        for column in range(self.size):
            new_queen = Queen(row, column)
            if self.is_valid_placement(new_queen):
                self.queens.append(new_queen)
                self.place_queen(row + 1)
                self.queens.pop()

    def is_valid_placement(self, new_queen):
        for queen in self.queens:
            if new_queen.is_attack(queen):
                return False
        return True


def print_solutions(solutions):
    for solution in solutions:
        board_size = len(solution)
        for column in solution:
            row = ["."] * board_size
            row[column] = "Q"
            print(" ".join(row))
        print()


# Example usage
n = 8
board = Board(n)
board.add_listener(print_solutions)
board.solve()
