# Key Word in Context (KWIC). Pipes-and-filters Method

class KWIC:
    def __init__(self, text):
        self.text = text
        self.filters = []
    
    def add_filter(self, filter_func):
        self.filters.append(filter_func)
    
    def process(self):
        result = self.text
        for filter_func in self.filters:
            result = filter_func(result)
        return result

# Filter functions
def extract_keywords(text):
    words = text.split()
    common_words = ["a", "an", "the", "in", "on", "of"]
    keywords = [word for word in words if word.lower() not in common_words]
    return keywords

def generate_context0(keywords, text):
    contexts = []
    for keyword in keywords:
        index = text.lower().index(keyword.lower())
        start_index = max(0, index - 10)
        end_index = min(len(text), index + len(keyword) + 10)
        contexts.append(text[start_index:end_index])
    return contexts


def generate_context(keywords, text):
    contexts = []
    for keyword in keywords:
        index = text.lower().index(keyword.lower())
        start_index = max(0, index - 10)
        end_index = min(len(text), index + len(keyword) + 10)
        keyword_context = text[start_index:end_index]
        contexts.append((keyword, keyword_context))
    return contexts

# Example usage
text = "This is a sample text. It contains keywords for testing."
kwic = KWIC(text)
kwic.add_filter(extract_keywords)
kwic.add_filter(lambda keywords: generate_context(keywords, text))
result = kwic.process()
print(result)
