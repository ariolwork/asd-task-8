# Eight Queens (8Q). Abstract Data Types Method

class Board:
    def __init__(self, size):
        self.size = size
        self.board = [[False] * size for _ in range(size)]
        self.solutions = []
    
    def is_safe(self, row, col):
        # Check if no queens are attacking in the same row
        for i in range(col):
            if self.board[row][i]:
                return False
        
        # Check if no queens are attacking in the upper diagonal
        for i, j in zip(range(row, -1, -1), range(col, -1, -1)):
            if self.board[i][j]:
                return False
        
        # Check if no queens are attacking in the lower diagonal
        for i, j in zip(range(row, self.size), range(col, -1, -1)):
            if self.board[i][j]:
                return False
        
        # If no queens are attacking, it is safe to place a queen
        return True
    
    def solve(self):
        self._solve_util(0)
        if not self.solutions:
            print("No solutions found.")
        else:
            for solution in self.solutions:
                self.print_solution(solution)
    
    def _solve_util(self, col):
        if col == self.size:
            self.solutions.append([row[:] for row in self.board])
            return
        
        for row in range(self.size):
            if self.is_safe(row, col):
                self.board[row][col] = True
                self._solve_util(col + 1)
                self.board[row][col] = False
    
    def print_solution(self, solution):
        for row in solution:
            for cell in row:
                if cell:
                    print("Q", end=" ")
                else:
                    print(".", end=" ")
            print()
        print()


# Example usage
board = Board(8)
board.solve()
